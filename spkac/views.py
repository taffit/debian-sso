from django.conf import settings
from django.utils.translation import ugettext_lazy as _
from django.views.generic import View, TemplateView
from django.views.generic.edit import CreateView
from django.utils.crypto import get_random_string
from django.utils import timezone
from django.shortcuts import redirect
from django.utils.decorators import method_decorator
from django.contrib.auth.decorators import login_required
from django.contrib.auth import get_user_model
from django import forms
from django import http
import os
import shlex
from ca.models import Certificate
from ca.ca import CA, CommandError


class SPKACMixin(object):
    def get_context_data(self, **kw):
        ctx = super(SPKACMixin, self).get_context_data(**kw)
        ctx["domain"] = self.domain
        ctx["name"] = self.request.user.get_username()
        return ctx

    @method_decorator(login_required)
    def dispatch(self, request, *args, **kw):
        self.domain = self.kwargs["domain"]
        return super(SPKACMixin, self).dispatch(request, *args, **kw)


class StatusView(SPKACMixin, TemplateView):
    template_name = "spkac/status.html"

    def get_context_data(self, **kw):
        ctx = super(StatusView, self).get_context_data(**kw)
        ctx["certs"] = Certificate.objects.filter(user=self.request.user).order_by("-generated")
        return ctx


class EnrollForm(forms.ModelForm):
    class Meta:
        model = Certificate
        fields = ["validity", "comment"]


class EnrollView(SPKACMixin, CreateView):
    template_name = "spkac/enroll.html"
    form_class = EnrollForm

    def get_context_data(self, **kw):
        ctx = super(EnrollView, self).get_context_data(**kw)
        challenge = self.request.session.get("challenge", None)
        if challenge is None:
            self.request.session["challenge"] = challenge = get_random_string(length=32)
        ctx["challenge"] = challenge
        return ctx

    def form_valid(self, form):
        spkac = self.request.POST.get("key", None)
        if spkac is None:
            form.add_error(None, "The browser does not seem to support the <keygen> feature")
            return self.form_invalid(form)
        challenge = self.request.session.get("challenge", None)
        if challenge is None:
            form.add_error(None, "I could not find the challenge string in the visiting session: please check that you have cookies enabled, and that your browser supports httponly cookies.")
            return self.form_invalid(form)
        cert = form.save(commit=False)
        cert.user = self.request.user

        ca = CA()
        try:
            ca.make_spkac_certificate(
                spkac=spkac,
                challenge=challenge,
                cert=cert)
        except RuntimeError as e:
            form.add_error(None, "Validation failed: " + str(e))
            return self.form_invalid(form)
        except CommandError as e:
            form.add_error(None, "{} failed".format(e.cmd[0]))
            return self.form_invalid(form)

        cert.save()

        del self.request.session["challenge"]

        if self.request.POST.get("manual", None) is not None:
            name = self.request.user.email
            res = http.HttpResponse(content=cert.pem, content_type="application/octet-stream")
            res["Content-Disposition"] = 'attachment; filename="{}"'.format(name + ".crt")
            return res
        else:
            return http.HttpResponse(content=cert.der, content_type="application/x-x509-user-cert")


class EnrollManuallyView(EnrollView):
    def get_context_data(self, **kw):
        ctx = super(EnrollManuallyView, self).get_context_data(**kw)
        ctx["manual"] = True
        return ctx


class EnrollCSRView(SPKACMixin, CreateView):
    template_name = "spkac/enroll_csr.html"
    form_class = EnrollForm

    def get(self, request, *args, **kw):
        from django.middleware.csrf import get_token
        if request.GET.get("format") == "json":
            return http.JsonResponse({
                "subject": self._get_subject(),
                "csrf_token": get_token(request),
            })
        else:
            return super().get(request, *args, **kw)

    def _get_subject(self):
        ca = CA()
        userid = getattr(self.request.user, Certificate.get_userid_field_name())
        return "/CN={}/O={}".format(userid, ca.organization)

    def get_context_data(self, **kw):
        ctx = super().get_context_data(**kw)
        ctx["subject"] = shlex.quote(self._get_subject())
        return ctx

    def form_valid(self, form):
        csr = self.request.POST.get("csr")
        if csr is None:
            form.add_error(None, "Certificate Signing Request is missing")
            return self.form_invalid(form)
        cert = form.save(commit=False)
        cert.user = self.request.user

        ca = CA()
        try:
            ca.make_csr_certificate(csr_pem=csr, cert=cert)
        except RuntimeError as e:
            form.add_error(None, "Validation failed: " + str(e))
            return self.form_invalid(form)
        except CommandError as e:
            form.add_error(None, "{} failed".format(e.cmd[0]))
            return self.form_invalid(form)

        cert.save()

        name = self.request.user.email
        res = http.HttpResponse(content=cert.pem, content_type="application/octet-stream")
        res["Content-Disposition"] = 'attachment; filename="{}"'.format(name + ".crt")
        return res


class Revoke(SPKACMixin, View):
    def post(self, request, domain, serial):
        cert = Certificate.objects.get(serial=int(serial), user=request.user)
        if not cert.expired:
            ca = CA()
            ca.revoke(cert)
            cert.revoked = timezone.now()
            cert.save()
        return redirect("spkac_status", domain=self.domain)
