




import json
from django.http import HttpResponse

def _dump_args(request, tag):
    pass
    import datetime, os
    now = datetime.datetime.utcnow()
    with open("/tmp/sso-log-" + tag, "at") as fd:
        os.fchmod(fd.fileno(), 0o640)
        print("--- {}".format(now.strftime("%Y-%m-%d %H:%M:%S")), file=fd)
        for k, v in request.GET.items():
            print("GET {} -> {}".format(k, v), file=fd)
        for k, v in request.POST.items():
            print("POST {} -> {}".format(k, v), file=fd)
        for k, v in request.environ.items():
            print("ENV {} -> {}".format(k, v), file=fd)
        for k, v in request.COOKIES.items():
            print("COOKIE {} -> {}".format(k, v), file=fd)

def inspect_remote_user(request):
    return HttpResponse(request.META.get("REMOTE_USER", "no remote user"), content_type="text/plain")
