# file:///usr/share/doc/fabric/html/tutorial.html

from fabric.api import local, run, sudo, cd, env
import git

REMOTE_USER = "debsso"

env.hosts = ["diabelli.debian.org"]

def prepare_deploy():
    #local("./manage.py test my_app")
    #local("git add -p && git commit")
    local("test `git ls-files -cdmu | wc -l` = 0")
    local("git push")

def deploy():
    prepare_deploy()
    repo = git.Repo()
    current_commit = repo.head.commit.hexsha

    deploy_dir = "/srv/sso.debian.org/debsso"
    with cd(deploy_dir):
        sudo("git fetch", user=REMOTE_USER)
        sudo("test `git show-ref -s origin/master` = " + current_commit, user=REMOTE_USER)
        sudo("git rebase origin/master", user=REMOTE_USER)
        sudo("./manage.py collectstatic --noinput", user=REMOTE_USER)
        sudo("./manage.py migrate", user=REMOTE_USER)
        sudo("psql service=debsso -c 'grant select,insert,update,delete on all tables in schema public to debssoweb'",
             user=REMOTE_USER)
        sudo("psql service=debsso -c 'grant usage on all sequences in schema public to debssoweb'", user=REMOTE_USER)
        sudo("touch debsso/wsgi.py", user=REMOTE_USER)
