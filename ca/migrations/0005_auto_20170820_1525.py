# -*- coding: utf-8 -*-
# Generated by Django 1.10.7 on 2017-08-20 15:25
from __future__ import unicode_literals

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('ca', '0004_remove_certificate_serial'),
    ]

    operations = [
        migrations.RenameField(
            model_name='certificate',
            old_name='serial_text',
            new_name='serial',
        ),
    ]
